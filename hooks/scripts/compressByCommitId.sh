#!/bin/bash
read -p "Please paste your commit Id here  : " commitId
echo "Hi, $commitId. Determining files that changed till now"
git log --name-only $commitId...HEAD | grep 'profiles\|permissionsets' > tempfile.txt
echo "We are going to determine all the files changed between the $commitId and HEAD"
echo "The below files will be compressed"
while IFS=$'\n' read line
do
    name="\"$line\""
    echo $name
    echo "perl -i.bak -p .git/hooks/scripts/profileCompress.pl $name" >> runtemp.sh
done < tempfile.txt
while true; do
    read -p "Do you wish to proceed to compress ?" yn
    case $yn in
        [Yy]* ) echo 'cool, lets start'; sh runtemp.sh; rm runtemp.sh tempfile.txt profiles/*.bak permissionsets/*.bak > .ignore ; rm .ignore; break;;
        [Nn]* ) echo 'ok i wont do anything';rm runtemp.sh tempfile.txt; exit;;
        * ) echo "Please answer yes or no.";;
    esac
done
echo "Done"