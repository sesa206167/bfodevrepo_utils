echo 'deleting previous backups, I know I have an OCD'
echo 'THIS IS ONLY FOR PROFILES'
echo '========================='
rm -rf profiles/*.bak
echo 'starting to compress the PROFILE xml files, it saves space and reduces conflict for others'
echo 'Fair warning before I begin, its gonna take atleast 40-50 seconds on a mac, may be around 3 minutes on windows'
echo 'compressing files first to be able to remove lines easily later'
perl -i.bak -p scripts/profileCompress.pl profiles/*.profile
echo '2 times a charm to avoid any additional spaces or missing bits'
perl -i.bak -p scripts/profileCompress.pl profiles/*.profile
echo 'removing the backup files generated'
rm -rf profiles/*.bak 
echo 'Now just retaining true values'
perl -i.bak -p scripts/removeFalseValues.pl profiles/*.profile
echo 'we finished generating compressed files, lets delete the .bak files, you wont need them'
rm -rf profiles/*.bak
echo 'done'