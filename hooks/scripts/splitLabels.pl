use Data::Dumper; 


# my $inFile = "./labels/CustomLabels.labelsSmall";
my $inFile = $ARGV[0];
print "Processing ".$inFile."\n";
my $outPath = "./splits/labels";




open( INFILE, $inFile ) or die "cant open infile $inFile";

@lines = <INFILE>;

close INFILE; 

# Equivalent of console.log
# print Dumper( \@lines );

my $inLabel = 0;
my @buffer = ();
my $fname = "";
while ( $line = shift @lines ) {
		
	if ( $inLabel eq 0 ) {
		if ( $line =~ /<labels>/ ) {
			$inLabel = 1;
		}
	}
	if ( $inLabel eq 1 ) {
		push @buffer, $line; 
		
		if ( $line =~ /<fullName>(.*)<\/fullName>/ ) {
			$fname = $1; 
		}
		
		if ( $line =~ /<\/labels>/ ) {
			$inLabel = 0;
			
			# OUTPUT FOR DEBUG
			
			# print "FILE NAME $fname\n";
			# print "FILE CONTENT START\n";
			# print join('', @buffer );
			# print "FILE CONTENT END\n";
			my $outFile = ">".$outPath."/".$fname.".xml";
			open( OUTFILE, $outFile ) or die "cant open outfile $outFile";; 
			print OUTFILE join('', @buffer );
			close OUTFILE; 
			
			@buffer = (); 
			
		}
	}
	
}
