echo 'deleting previous backups, I know I have an OCD'
echo 'THIS IS ONLY FOR PERMISSION SETS'
echo '========================='
rm -rf permissionsets/*.bak
echo 'starting to compress the xml files, it saves space and reduces conflict for others'
echo 'Fair warning before I begin, its gonna take atleast 40-50 seconds on a mac, may be around 3 minutes on windows'
echo 'compressing files first to be able to remove lines easily later'
perl -i.bak -p .git/hooks/scripts/profileCompress.pl permissionsets/*.permissionset
echo '2 times a charm to avoid any additional spaces or missing bits'
perl -i.bak -p .git/hooks/scripts/profileCompress.pl permissionsets/*.permissionset
echo 'removing the backup files generated'
rm -rf permissionsets/*.bak 
echo 'Now just retaining true values'
perl -i.bak -p .git/hooks/scripts/removeFalseValues.pl permissionsets/*.permissionset
echo 'we finished generating compressed files, lets delete the .bak files, you wont need them'
rm -rf permissionsets/*.bak
echo 'done'