s/^<applicationVisibilities\>.*<visible\>false(.|\n)*//; 
s/^<classAccesses\>.*<enabled\>false(.|\n)*//; 
s/^<pageAccesses\>.*<enabled\>false(.|\n)*//; 
s/^<recordTypeVisibilities\>.*<visible\>false(.|\n)*//; 
s/^<tabVisibilities\>.*<visibility\>Hidden(.|\n)*//; 
s/^<tabSettings\>.*<visibility\>None(.|\n)*//; 
s/^<fieldPermissions\>.*<editable\>false.*<readable\>false(.|\n)*//; 
s/^<objectPermissions\>.*<allowCreate\>false.*<allowDelete\>false.*<allowEdit\>false.*<allowRead\>false.*<modifyAllRecords\>false.*<viewAllRecords\>false(.|\n)*//;